# For a container with common security applications

# Start with our Kuali base image
FROM easksd/base
MAINTAINER U of A Kuali DevOps <katt-support@list.arizona.edu>

# Copy in start script and make executable
COPY bin /usr/local/bin/
RUN chmod +x /usr/local/bin/*

# update sources because of package location error for fail2ban
RUN apt-get update

# Download and install iptables
RUN apt-get install -y iptables
COPY configurations/iptables/iptables-rules /etc/iptables.rules

# Download and install fail2ban
RUN apt-get install -y fail2ban
COPY configurations/fail2ban/jail.local /etc/fail2ban

ENTRYPOINT /usr/local/bin/start-security-apps.sh