# University of Arizona Kuali Security Docker Container
---

### Description
This project defines an image that contains security applications for our Kuali Docker containers.

### Building
A sample build command is: `docker build -t kuali/ksd-security --force-rm .`

This command will build an image and tag it with the name ksd-security.

### Running
A sample run command (used for testing) is: `docker run -d --name=kuali-security --privileged=true kuali/ksd-security`

This command will run a container called kuali-security based on the ksd-security image. It needs to be run in privileged mode.

### Technical Details From Development

##### iptables
Needed for fail2ban. Starting it up for when we switch to CentOS. When running on ubuntu, no explicit startup is needed as it is a kernel process.

The configuration file, iptables-rules, is a copy of the one that is used in the sftp-puppet project. More details about that are at https://bitbucket.org/ua-ecs/sftphub-puppet.

##### fail2ban
The configuration file, jail.local, is a copy of the one that is used in the sftp-puppet project. More details about that are at https://bitbucket.org/ua-ecs/sftphub-puppet.

##### rsyslogd
We are running rsyslogd. More details about that are at http://stackoverflow.com/questions/22526016/docker-container-sshd-logs.
