#!/bin/bash

# Starting system logging explicitly to get around startup error
touch /var/log/auth.log && chmod a+w /var/log/auth.log
touch /var/log/secure && chmod a+w /var/log/secure
rsyslogd

# establish networking rules
iptables-restore < /etc/iptables.rules

# start iptables
/etc/init.d/iptables start

# start fail2ban
/etc/init.d/fail2ban restart

# tail a log so container doesn't exit
tail -f /var/log/auth.log
